# README #

### Sample Grouper API script

The `examples.py` Python script illustrates using the Grouper Web Services "Lite" API for the following use cases:

- retrieve all members of a group
- retrieve all groups for a particular subject
- check if a subject is a member of a group

Master documentation for the Grouper Web Services API can be found at [https://spaces.internet2.edu/display/Grouper/Grouper+Web+Services].

If you encounter SSL certificate errors when connecting to `https://grouper.arizona.edu`, use the `examples.sh` wrapper.

This script requires installation of the Python [requests module](http://docs.python-requests.org/en/master/), which can typically be installed by performing `pip install requests`.
