#!/usr/bin/python

import re
import requests

## Grouper params
# hostname of Grouper WS endpoint
grouper_host = 'grouper.arizona.edu'
# base URI for Grouper WS
grouper_base_path = '/grouper-ws/servicesRest/json/v2_2_001'
# stem name
grouper_stem = 'arizona.edu:services:enterprise:cherwell:service-desk'
# group name
group = 'LAW'
# subject UAID
subject = '108646680292'
# credentials for Grouper WS API user
grouper_user = 'grouper-api-username'
grouper_pass = 'grouper-api-password

# get all members of a group
r = requests.get("https://%s%s/groups/%s:%s/members" % (grouper_host, grouper_base_path, grouper_stem, group), auth=(grouper_user, grouper_pass))
grouper_resp = r.json()
if 'wsSubjects' in grouper_resp['WsGetMembersLiteResult']:
    print "members of group %s:%s:" % (grouper_stem, group)
    for rec in grouper_resp['WsGetMembersLiteResult']['wsSubjects']:
        if re.search('^\d{12}$', rec['id']) and rec['sourceId'] == "ldap":
            print "\t%s" % rec['id']
    print ""

# get all groups for subject
r = requests.get("https://%s%s/subjects/%s/groups" % (grouper_host, grouper_base_path, subject), auth=(grouper_user, grouper_pass))
grouper_resp = r.json()
if 'wsGroups' in grouper_resp['WsGetGroupsLiteResult']:
    print "subject %s groups:" % subject
    for rec in grouper_resp['WsGetGroupsLiteResult']['wsGroups']:
        print "\t%s" % rec['name']
    print ""

# check if group has subject as a member
r = requests.get("https://%s%s/groups/%s:%s/members/%s" % (grouper_host, grouper_base_path, grouper_stem, group, subject), auth=(grouper_user, grouper_pass))
grouper_resp = r.json()
if 'resultMetadata' in grouper_resp['WsHasMemberLiteResult']:
    status = "IS NOT"
    if grouper_resp['WsHasMemberLiteResult']['resultMetadata']['resultCode'] == 'IS_MEMBER':
        status = "IS"
    print "%s %s a member of group %s:%s" % (subject, status, grouper_stem, group)
